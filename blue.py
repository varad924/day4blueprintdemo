from flask import Blueprint, render_template

blue = Blueprint("blue", __name__, template_folder="templates")


@blue.route("/login")
@blue.route("/")
def login():
    return render_template("login.html")
