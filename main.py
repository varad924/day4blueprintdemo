from flask import *
from blue import blue

app = Flask(__name__)
app.register_blueprint(blue, url_prefix="/login")
app.secret_key = "jarvis"


@app.route('/')
def home():
    return render_template("front.html")


@app.route('/done', methods=["POST"])
def success():
    if request.method == "POST":
        session['email'] = request.form['email']
    return render_template('done.html')


@app.route('/logout')
def logout():
    if 'email' in session:
        session.pop('email', None)
        return render_template('logout.html');
    else:
        return '<p>user already logged out</p>'


@app.route('/profile')
def profile():
    if 'email' in session:
        email = session['email']
        return render_template('profile.html', name=email)
    else:
        return render_template('viewprofileerror.html')


if __name__ == '__main__':
    app.run(debug=True)
